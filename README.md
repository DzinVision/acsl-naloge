ACSL naloge
===========

Originalne naloge so v mapisah po letih, leto 2013/14 in 2014/15 je precej polno, allstar tekmovanja
imamo iz leta 2012/13 in 2013/14.

Moje naloge so v mapi `naloge`, v kateri si zbrane vse
programerske in teoretične naloge, ki sem jih zbral ali si jih zmislil.
Vse so zbrane v zbriki nalog, ponovljene pa so tudi v kakšnih domačin nalogah.

V mapi `snov` je uradna snov in nekaj malega dodatnega materiala.

Priporočam se za rešitve :)

Jure Slak
