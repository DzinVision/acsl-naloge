# Jure Slak
# Python, version 3.3

transform = lambda x: [((t-1)%5,(t-1)//5) for t in x] # transformaciji v običajni koordinatni sistem
revtransform = lambda x: [1+u+v*5 for u,v in x]
valid = transform(range(1,26))

def kill(x, y, dx, dy): # pobije v smeri dx, dy in vrne seznam mest
    move = []
    for i in range(1,6):
        c = (x+i*dx, y+i*dy)
        if c not in black: return move
        move.append(c)
    return move

for t in range(5):
    k = list(map(int, input().split()))
    white = set(transform(k[1:k[0]+1]))
    black = set(transform(k[k[0]+2:k[0]+k[k[0]+1]+2]))
   
    maxmove = []
    for x,y in white:  # dodamo osnovne smeri + ostale veljavne smeri za ta x, y
        dirs = [(-1,0),(1,0),(0,-1),(0,1)] + ([(-1,-1),(1,1)] if x==y else []) + ([(-1,1),(1,-1)] if y+x==4 else [])
        for dx, dy in dirs:
            c = (x+dx,y+dy)
            if c in valid and c not in black | white: # se smemo premaknit v tej smeri
                maxmove = max(kill(x+dx,y+dy,dx,dy),kill(x,y,-dx,-dy),maxmove,key=len) # ubijamo v obe smeri

    print (', '.join(map(str, sorted(revtransform(maxmove)))) if maxmove != [] else 'NONE')
