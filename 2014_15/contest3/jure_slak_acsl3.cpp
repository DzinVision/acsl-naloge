#include <iostream>
#include <cstring>

using namespace std;

int getx(int n) { return (n-1) % 7; }
int gety(int n) { return (n-1) / 7; }
int back(int x, int y) { return 7*y + x + 1; }

int main() {

    int grid[7][7];
    memset(grid, 0, sizeof grid);

    int dirs[][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}, {1, -1}, {-1, 1}, {-1, -1}, {1, 1}};

    int prvi, drugi, koor;
    for (int i = 0; i < 5; ++i) {
        memset(grid, 0, sizeof grid);
        cin >> prvi >> drugi >> koor;
        grid[gety(prvi)][getx(prvi)] = 1;
        while (koor) {
            grid[gety(koor)][getx(koor)] = 1;
            cin >> koor;
        }

        int bestdir[2];
        int maxc = 0;
        for (int* dir : dirs) {
            int c = 0;
            int sx = getx(drugi), sy = gety(drugi);
            while (0 <= sx && sx < 7 && 0 <= sy && sy < 7 && grid[sy][sx] == 0)
                sx += dir[0], sy += dir[1], c++;
            if (c > maxc)
                maxc = c, bestdir[0] = dir[0], bestdir[1] = dir[1];
        }

        int sx = getx(drugi), sy = gety(drugi);
        for (int j = 1; j < maxc; ++j)
            cout << back(sx + bestdir[0]*j, sy + bestdir[1]*j) << ' ';
        if (maxc == 1) cout << "NONE";
        cout << '\n';
    }
    return 0;
}

